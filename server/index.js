const express = require("express");
//CORS is a needed library to unblock the request to external servers
var cors = require("cors");
const bodyParser = require("body-parser");

//var router = express.Router();

const app = express();
const port = process.env.PORT || 4000;

app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
//get data for the assets grid
var request = require("request");
app.get("/assets", function(req, res) {
  request(
    "https://6y458uslg3.execute-api.eu-west-3.amazonaws.com/elixos/assets",
    function(error, response, body) {
      //console.log(response);
      if (error) console.log("error:", error); // Print the error if one occurred and handle it
      console.log("statusCode:", response && response.statusCode); // Print the response status code if a response was received
      res.send(body);
    }
  );
});
//get data for the entities grid
app.get("/entities/:id", function(req, res) {
  request(
    "https://6y458uslg3.execute-api.eu-west-3.amazonaws.com/elixos/entities",
    function(error, response, body) {
      if (error) console.log("error:", error);
      console.log("statusCode:", response && response.statusCode);
      res.send(body);
    }
  );
});

app.listen(port, () => console.log(`Listening on port ${port}`));
