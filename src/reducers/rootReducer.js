const initState = {
    assets: { id: "to_define" },
};

const rootReducer = (state = initState, action) => {
    switch (action.type) {
        case "SET_ASSET":
            return {
                ...state,
                assets: { id: action.id },
            };
        default:
    }
    return state;
};

export default rootReducer;
