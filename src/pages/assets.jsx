import React, { Component } from "react";
import axios from "axios";
import Grid from "../components/assetsGrid";
import { connect } from "react-redux";

class Assets extends Component {
    constructor(props) {
        super(props);
        this.state = { header: [], tdata: [] };
    }

    componentDidMount = async () => {
        //sendig a request to the express.js middleware with axios to get the grid data
        const data = await axios.get("http://localhost:4000/assets");
        const values = data.data.assets.map((val) => {
            return val;
        });
        this.setState({
            header: ["id", "t_street_name", "n_number", "t_city", "t_code"],
            tdata: values,
        });
    };

    goEntities = (asset_id) => {
        console.log("assets.jsx ~ line 25 ~ Assets ~ asset_id", asset_id);
        this.props.setAsset(asset_id);
        this.props.history.push("/entities");
    };

    render() {
        const { header, row, tdata } = this.state;
        return (
            <React.Fragment>
                <Grid key={tdata.id} row={row} header={header} tdata={tdata} goEntities={this.goEntities} />
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state, ownProps) => ({
    assets: state.assets,
});
const mapDispatchToProps = (dispatch) => {
    return {
        setAsset: (id) => {
            dispatch({ type: "SET_ASSET", id: id });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Assets);
