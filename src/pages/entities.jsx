import React, { Component } from "react";
//library to make ajax calls to the server
import axios from "axios";
import Grid from "../components/entitiesGrid";
import { connect } from "react-redux";

class Entities extends Component {
    constructor(props) {
        super(props);
        this.state = { header: [], tdata: [] };
    }
    //ES7 async awit
    componentDidMount = async () => {
        const { assets } = this.props;
        //get in the component the URL parameter that contain the asset ID cliked
        //the location prop stores thr URL
        const data = await axios.get(`http://localhost:4000/entities/${assets.id}`);
        console.log("🚀 ~ file: entities.jsx ~ line 20 ~ Entities ~ componentDidMount= ~ data", data);
        const fields = [];
        //I'm gonna use a secondary array called v to store the related entities
        //I supose that id_asset is the field that matches with the id of the assets
        const v = [];
        const values = data.data.entities.map((val) => {
            if (val.id_asset === assets.id) {
                //Using directly the values because there are to many fields
                fields.push(Object.keys(val));
                v.push(Object.values(val));
                return val;
            }
            return;
        });
        this.setState({ header: fields[0], tdata: v });
    };

    render() {
        const { header, tdata } = this.state;
        return (
            <React.Fragment>
                <Grid header={header} tdata={tdata} />
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    assets: state.assets,
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Entities);
