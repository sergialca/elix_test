import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import assets from "./pages/assets";
import entities from "./pages/entities";
import "./App.css";

function App() {
    return (
        <div>
            <Switch>
                <Route path="/assets" component={assets} />
                <Route path="/entities" component={entities} />
                <Redirect from="/" to="/assets" />
            </Switch>
        </div>
    );
}

export default App;
