import React from "react";

const assetsGrid = ({ header, tdata, goEntities }) => {
    return (
        <div>
            <table id="assets" className="table table-striped">
                <thead>
                    <tr>
                        {header.map((field) => (
                            <th>{field}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {tdata.map((el) => (
                        <tr key={el.id}>
                            <td>{el.id}</td>
                            <td>{el.t_street_name}</td>
                            <td>{el.n_number}</td>
                            <td>{el.t_city}</td>
                            <td>{el.t_code}</td>
                            <td>
                                <button onClick={() => goEntities(el.id)}>Entities</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default assetsGrid;
