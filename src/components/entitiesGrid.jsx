import React from "react";
import ContextMenu from "react-context-menu";
const entitiesGrid = ({ header, tdata }) => {
  let id = 0;
  let cont = 0;
  const rowClick = el => {
    //A little bit simplier
    //console.log("Entity ID", Object.values(el)[0][0]);
    id = Object.values(el)[0][0];
  };
  const test = () => {
    console.log("entity ID ", id);
  };
  const suma = () => {
    cont++;
  };
  return (
    <div>
      {/*this table uses bootrstrap clases (clanName)*/}
      <ContextMenu
        contextId={"entities"}
        closeOnClick
        //onClick={el => test(el)}
        items={[{ label: "Test", onClick: () => test() }]}
      />
      <table id="entities" className="table table-striped">
        <thead>
          <tr>
            {header.map(field => (
              <th>{field}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {tdata.map(el => (
            <tr key={cont} onClick={suma()}>
              {el.map(val => (
                <td
                  onContextMenu={e => rowClick({ el }, e)}
                  onClick={e => rowClick({ el }, e)}
                >
                  {val}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export default entitiesGrid;
